import { createWidget } from "discourse/widgets/widget";
import { h } from "virtual-dom";

createWidget("user-login-info", {
  tagName: "div",
  buildKey: () => "user-custom-info",
  defaultState() {
    return {
      loaded: false,
      email: "",
    };
  },

  getUserEmail() {
    fetch(
      `${window.location.origin}/users/${this.currentUser.username}/emails.json`
    )
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.state.email = data.email;
        this.state.loaded = true;
        this.scheduleRerender();
      });
  },

  html() {
    const elements = [];
    if (!this.state.loaded && this.currentUser?.username) {
      this.getUserEmail();
    }
    if (this.state.loaded) {
      elements.push(
        h("span", "Logged in as"),
        h("span.mx-2", this.state.email),
        h(
          "a.mx-2.underline",
          {
            href: "https://support.primero.org/logout",
          },
          "Logout"
        )
      );
      if (this.currentUser.admin) {
        elements.push(
          h(
            "a.underline.mr-2.text-white",
            {
              href: "https://support.primero.org/admin",
            },
            "Admin"
          )
        );
      }
    }

    if (!this.currentUser) {
      elements.push(h("span.mr-2", "You are not logged in"));
    }
    return h("div", elements);
  },
});
