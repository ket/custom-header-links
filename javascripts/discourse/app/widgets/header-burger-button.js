import { createWidget } from "discourse/widgets/widget";
import { h } from "virtual-dom";

createWidget("header-burger-button", {
  tagName: "button.hamburgerButton.lg:hidden.absolute.z-40",

  html() {
    return h("img", {
      src: "http://discourse/uploads/default/original/1X/4cac6b4a1ebeacef1ec41c7bdf21941d569d3755.png",
    });
  },
  click(event) {
    event.stopPropagation();
    const navMenu = document.querySelector(
      "header nav[data-custom-navigation]"
    );
    if (navMenu) {
      navMenu.classList.toggle("invisible");
    }
  },
});
