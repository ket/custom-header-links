import { createWidget } from "discourse/widgets/widget";
import hbs from "discourse/widgets/hbs-compiler";
import { withPluginApi } from "discourse/lib/plugin-api";

createWidget("header-contents", {
  tagName: "div.contents.clearfix",
  template: hbs`
    {{header-burger-button attrs=attrs}}
    <nav data-custom-navigation="" class="invisible lg:visible bg-white fixed lg:relative inset-0 lg:inset-auto z-10 lg:z-auto flex flex-col lg:flex-row self-stretch justify-start lg:justify-between flex-1">
    <a href="https://support.primero.org" title="Home page" class="self-center">
    <img src="http://discourse/uploads/default/original/1X/a32c0b7bcc9e04b1c684c7ded643ce13babb4dcf.png" alt="Primero">
    </a>
      <ul class="primeroNavMenu text-base flex-col lg:flex-row flex lg:items-center">
        <li class="my-2 mx-2">
          <a class="m-1 font-bold uppercase" href="https://support.primero.org">HOME</a>
        </li>
        <li class="my-2 mx-2">
          <a class="m-1 font-bold uppercase" href="https://support.primero.org/releases">RELEASES</a>
        </li>
        <li class="my-2 mx-2">
          <a class="m-1 font-bold uppercase" href="https://support.primero.org/documentation">DOCUMENTATION</a>
        </li>
        <li class="my-2 mx-2">
          <a class="m-1 font-bold uppercase" href="https://support.primero.org/support">SUPPORT</a>
        </li>
        <li class="my-2 mx-2">
          <a class="m-1 font-bold uppercase" href="https://community.primero.org/">COMMUNITY</a>
        </li>
      </ul>
      </nav>
      {{try-modal attrs=attrs}}
      {{try-button attrs=attrs}}
      {{#if attrs.topic}}
      {{header-topic-info attrs=attrs}}
      {{/if}}
      <div class="leftBorder self-stretch py-4 lg:p-0 lg:my-4 panel clearfix">{{yield}}</div>
      `,
});

withPluginApi("0.1", (api) => {
  api.decorateWidget("header:after", (helper) => {
    return helper.h(
      "div.userStatus.coloredBG.py-2.w-full.text-white.font-normal.text-sm.flex.justify-end",
      helper.attach("user-login-info")
    );
  });
});
